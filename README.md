Global Golub-Kahan bidiagonalization applied to large discrete ill-posed problems and Gauss-type quadrature rules to restore a blurred and noisy image. "Global_Golub_Kahan_Image_Restoration" is a Matlab package for image restoration 

THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
ANY USE OF THE SOFTWARE CONSTITUTES  ACCEPTANCE OF THE TERMS OF THE ABOVE STATEMENT.

# Authors 

 K. JBILOU                                                            
     Université du Littoral, Calais, France                               
     E-mail: khalide.jbilou@univ-littoral.fr   

M. ELGUIDE
     Université polytechnique Mohammed VI, Maroc
