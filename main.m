%% Script that use the function GGKB based on the connection
%  between Global Golub-Kahan bidiagonalization algorithm and Gauss-type quadrature
%  rules to restore a blurred and noisy color image 
%  Reference : A. H. Bentbib, M. El Guide, K. Jbilou, and L. Reichel, 
%              Global Golub-Kahan bidiagonalization applied to large 
%              discrete ill-posed problems,Journal of Computational and 
%                Applied Mathematics, 322(2017), pp. 46-56
clear all;
clc;
EXACTSOL=double(imread('iogray.tif'));% original image
%EXACTSOL=double(imread('enamel.tif'));
EXACTSOL=EXACTSOL(1:256,1:256);
[n,n]=size(EXACTSOL);

%% forming the bluring matrices H1 and H2
sigma=2.5;
t=6;
for i=1:n
    for j=1:n
        if abs(i-j)<= t;
            H1(i,j)=(1/(2*sqrt(2*pi)*sigma))*exp(-(i-j)^2/(2*sigma^2));
        else 
           H1(i,j)=0;
        end
    end
end
H1=sparse(H1);
H2=H1;
%%
noisel=0.001; % noise level
B_exact=H1*EXACTSOL*H2'; % Error free right hand side
E = randn (size(B_exact));
E = E / norm(E,'fro') ; 
B = B_exact+noisel*norm(B_exact,'fro')*E;%error contaminated right hand side
s=norm(noisel*norm(B_exact,'fro')*E,'fro'); % norm of the error
%*******************************************************************

eta=1.1; % safety factor
mu0=10; %initialization of mu


%computes the regularization parameter and the Tikhonov regularized  
% solution using Global Golub-Kahan bidiagonalization (GGKB) approach
tic
[X,mu,err_r,l]=GGKB(B,EXACTSOL,H1,H2,mu0,eta,s);
time=toc
GGKB_steps=l
err_r
mu
subplot(131);imshow(EXACTSOL,[]);title('Original Image');
subplot(132);imshow(B,[]);title('Blurred and Noisy Image');
subplot(133);imshow(X,[]);title('Restored Image');
%*******************************************************************

