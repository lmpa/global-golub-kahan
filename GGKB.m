
function [X,mu,err_r,l]=GGKB(B,EXACTSOL,H1,H2,mu0,eta,s)
%------------------------------------------------%
% This function computes the regularization parameter and the Tikhonov 
%  regularized  solution to the problem H1*X*H2'=B+E, using the identity
%  matrix as the regularization operator.
%  Input: 
%          B - blurred image
%          EXACTSOL - original image
%          H1, H2= blurring matrices
%          mu0= initialization of the regularization parameter
%          eta= safety factor for the discrepancy principle
%          s= norm of the additive error

%  Output: 
%          X - restored image
%          mu=computed regularization parameter
%        err_r= relative error
%          l=GGKB steps
%  Reference : A. H. Bentbib, M. El Guide, K. Jbilou, and L. Reichel, 
%              Global Golub-Kahan bidiagonalization applied to large 
%              discrete ill-posed problems,Journal of Computational and 
%                Applied Mathematics, 322(2017), pp. 46-56

n=size(EXACTSOL,1);
q=size(B,2);
l=2;
t=norm(B,'fro')^2;
sigma1=norm(B,'fro');
U1=B/sigma1;
rho1=norm(t_oper1(H1,H2,U1),'fro');
V1=t_oper1(H1,H2,U1)/rho1;
V(:,1:q)=V1;
C_gauss(1,1)=rho1;
W=oper1(H1,H2,V1)-rho1*U1;
sigma=norm(W,'fro');
U1=W/sigma;
C_gauss(2,1)=sigma;
W=t_oper1(H1,H2,U1)-sigma*V1;
rho=norm(W,'fro');
C_gauss(2,2)=rho;
V1=W/rho;
V(:,q+1:2*q)=V1;
mu=newton(C_gauss',l,t,mu0,eta,s);
W=oper1(H1,H2,V1)-rho*U1;
sigma=norm(W,'fro');
U1=W/sigma;
C_gauss(3,2)=sigma;
C_radau=C_gauss;
 while phi_radau(C_radau',l,mu,t)>eta^2*s^2 
     l=l+1;
W=t_oper1(H1,H2,U1)-sigma*V1;
rho=norm(W,'fro');
C_gauss(l,l)=rho;
V1=W/rho;
V(:,(l-1)*q+1:l*q)=V1;
mu=newton(C_gauss',l,t,mu,eta,s);
W=oper1(H1,H2,V1)-rho*U1;
sigma=norm(W,'fro');
U1=W/sigma;
C_gauss(l+1,l)=sigma;
C_radau=C_gauss;
 end

%y=inv(C_radau'*C_radau+(1/mu)*eye(l,l))*sigma1*rho1*eye(l,1);
D=[sqrt(mu)*C_radau;eye(l)];
d=[sqrt(mu)*sqrt(t)*eye(l+1,1);zeros(l,1)];
[Q,R]=qr(D);
y=R\(Q'*d);
C=kron(y,sparse(eye(n)));
 X=V(:,1:l*n)*C;
 err_r=norm(EXACTSOL-X,'fro')/norm(EXACTSOL,'fro');